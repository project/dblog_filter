# DBLog Filter

This module allows you to store only filtered log messages based on
log type and log level.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/dblog_filter).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/dblog_filter).

## Contents of this file

- Features
- Requirements
- Installation
- Configuration
- Maintainers


## Features

- Useful to restrict unwanted message types and reduce dblog/syslog size for
  better performance.
- Majorly useful in production sites when we want to log only
  limited messages.
- Logging can be restricted either by the Levels. To Log only Error Level logs
  for a site, you can check "Errors" under "Select Severity Levels." in the
  configuration page.
- Custom logging can also be done ignoring all the core logs too, by setting
  the values under "Enter Log type and Severity Levels."


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Click on Configure link in modules page.
- Or Go to http://YOURSITE/admin/reports/dblog-filter , configure type and
  level.
- To restrict by Severity Level, Select from the allowed severity levels.
- To restrict by Custom Logging for DB Log, set it up under "Enter DB Log type
  and Severity Levels."
  Give one per line as (TYPE|LEVEL)
    - php|notice,error,alert
    - mymodule|notice,warning
  This only allows to log watchdogs of type "php" and "mymodule", with their
  respective severity levels.
  For example, Following are the Logs Recorded(as per the above settings):
  ```
  \Drupal::logger('mymodule')->notice('@build:',
   array('@build' => print_r($build, true)));
  ```
  ```
  \Drupal::logger('php')->alert('@build:',
  array('@build' => print_r($build, true)));
  ```

  Logs not recorded:
  ```
  \Drupal::logger('php')->warning('@build:',
  array('@build' => print_r($build, true)));
  ```


## Maintainers

- Harika Gujjula - [harika gujjula](https://www.drupal.org/u/harika-gujjula)
- Ajay Reddy - [ajay_reddy](https://www.drupal.org/u/ajay_reddy)
- kishorekumar Nagarajan - [N.kishorekumar](https://www.drupal.org/u/nkishorekumar)
- Rajeshwari Variar - [rajeshwari10](https://www.drupal.org/u/rajeshwari10)
