<?php

namespace Drupal\Tests\dblog_filter\Kernel;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\dblog_filter\Form\DBLogFilterSettingsForm;
use Drupal\dblog_filter\Logger\DBLogFilter;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the LogFilterTrait.
 *
 * @group dblog_filter
 */
class LogFilterTest extends KernelTestBase {

  /**
   * Log filter class.
   *
   * @var \Drupal\dblog_filter\Logger\DBLogFilter
   */
  protected DBLogFilter $dblogFilter;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'dblog_filter',
    'dblog',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['system', 'dblog_filter']);
    $this->installSchema('dblog', ['watchdog']);
    $connection = \Drupal::database();
    $parser = \Drupal::service('logger.log_message_parser');
    $this->dblogFilter = new DBLogFilter($connection, $parser);

  }

  /**
   * Should log method coverage.
   *
   * @covers \Drupal\dblog_filter\Logger\DBLogFilter::shouldLog
   * @dataProvider logProvider
   */
  public function testFilterShouldLog($type, $level, $message, $context, $config_values, $should_log) {
    $config = \Drupal::configFactory()->getEditable('dblog_filter.settings');
    foreach ($config_values as $key => $value) {
      $config->set($key, $value);
      if ($key == 'log_values') {
        DBLogFilterSettingsForm::setLogValuesRegex($value, $config, $key);
      }
    }

    $config->save();
    $this->dblogFilter->setConfig($config);

    if ($should_log) {
      $this->assertTrue($this->dblogFilter->shouldLog($type, $level, $message, $context));
    }
    else {
      $this->assertFalse($this->dblogFilter->shouldLog($type, $level, $message, $context));
    }
  }

  /**
   * Log data provider.
   */
  public function logProvider() {
    return [
      [
        'dblog',
        RfcLogLevel::WARNING,
        'This log should not be logged',
        [
          'channel' => 'cron',
        ],
        [
          'log_values' => [
            'cron|warning',
            'user|warning',
          ],
          'severity_levels' => [],
          'method' => 'exclude',
        ],
        FALSE,
      ],
      [
        'dblog',
        RfcLogLevel::WARNING,
        'This log should be logged',
        [
          'channel' => 'cron',
        ],
        [
          'log_values' => [
            'cron|warning',
            'user|warning',
          ],
          'severity_levels' => [],
          'method' => 'include',
        ],
        TRUE,
      ],
      [
        'dblog',
        RfcLogLevel::WARNING,
        'This log should not be logged',
        [
          'channel' => 'cron',
        ],
        [
          'log_values' => [
            'cron|warning',
          ],
          'severity_levels' => [],
          'method' => 'exclude',
        ],
        FALSE,
      ],
      [
        'dblog',
        RfcLogLevel::INFO,
        'This log should be logged',
        [
          'channel' => 'cron',
        ],
        [
          'log_values' => [
            'cron|warning',
          ],
          'severity_levels' => [],
          'method' => 'exclude',
        ],
        TRUE,
      ],
      [
        'dblog',
        RfcLogLevel::INFO,
        'This log should be logged',
        [
          'channel' => 'user',
        ],
        [
          'log_values' => [
            'user|info|Test .* pattern regex .*',
          ],
          'severity_levels' => [],
          'method' => 'exclude',
        ],
        TRUE,
      ],
      [
        'dblog',
        RfcLogLevel::INFO,
        'Test 30 pattern regex 22',
        [
          'channel' => 'user',
        ],
        [
          'log_values' => [
            'user|info|Test .* pattern regex .*',
          ],
          'severity_levels' => [],
          'method' => 'exclude',
        ],
        FALSE,
      ],
      [
        'dblog',
        RfcLogLevel::INFO,
        'This log should not be logged',
        [
          'channel' => 'user',
        ],
        [
          'log_values' => [
            'user|info|Test .* pattern regex .*',
          ],
          'severity_levels' => ['info' => TRUE],
          'method' => 'exclude',
        ],
        FALSE,
      ],
      [
        'dblog',
        RfcLogLevel::INFO,
        'This log should be logged',
        [
          'channel' => 'user',
        ],
        [
          'log_values' => [
            'user|info|Test .* pattern regex .*',
          ],
          'severity_levels' => ['info' => TRUE],
          'method' => 'include',
        ],
        TRUE,
      ],
      [
        'dblog',
        RfcLogLevel::INFO,
        'This log should be logged',
        [
          'channel' => 'user',
        ],
        [
          'log_values' => [
            'user|info|Test .* pattern regex .*',
          ],
          'severity_levels' => ['warning' => TRUE],
          'method' => 'exclude',
        ],
        TRUE,
      ],
      [
        'dblog',
        RfcLogLevel::INFO,
        'This log should not be logged',
        [
          'channel' => 'user',
        ],
        [
          'log_values' => [
            'user|info|Test .* pattern regex .*',
          ],
          'severity_levels' => ['warning' => TRUE],
          'method' => 'include',
        ],
        FALSE,
      ],
      [
        'dblog',
        RfcLogLevel::INFO,
        'This log should be logged',
        [
          'channel' => 'user',
        ],
        [
          'log_values' => [
            'user|info',
          ],
          'severity_levels' => [],
          'method' => 'include',
        ],
        TRUE,
      ],
      [
        'dblog',
        RfcLogLevel::INFO,
        'This log should not be logged',
        [
          'channel' => 'user',
        ],
        [
          'log_values' => [
            'user|info|Test .* pattern regex .*',
          ],
          'severity_levels' => [],
          'method' => 'include',
        ],
        FALSE,
      ],
      [
        'dblog',
        RfcLogLevel::INFO,
        'Test 30 pattern regex 22',
        [
          'channel' => 'user',
        ],
        [
          'log_values' => [
            'user|info|Test .* pattern regex .*',
          ],
          'severity_levels' => [],
          'method' => 'include',
        ],
        TRUE,
      ],
    ];
  }

}
