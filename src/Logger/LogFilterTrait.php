<?php

namespace Drupal\dblog_filter\Logger;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\RfcLogLevel;

/**
 * The LogFilter base class.
 */
trait LogFilterTrait {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|null
   */
  protected ?ConfigFactoryInterface $configFactory = NULL;

  /**
   * Module Configuration.
   *
   * @var \Drupal\Core\Config\Config|null
   */
  protected ?Config $configDbLogFilter = NULL;

  /**
   * Get config by name.
   */
  public function getConfig(string $config_name) {
    if ($this->configFactory instanceof ConfigFactoryInterface) {
      $this->setConfig($this->configFactory->get($config_name));
    }
    else {
      $this->setConfig(\Drupal::config($config_name));
    }
    return $this->configDbLogFilter;
  }

  /**
   * Set configuration.
   */
  public function setConfig(Config $config) {
    $this->configDbLogFilter = $config;
  }

  /**
   * Check if the filter should log.
   */
  public function shouldLog(string $type, $current_level, string|\Stringable $message, array $context) {
    $method = 'exclude';
    $severity_levels = [];
    $log_value_list = [];
    $regex = [];

    // Get Log Filter Settings.
    $config = $this->getConfig('dblog_filter.settings');
    // Get Severity levels Configuration.
    if ($type == 'dblog') {
      $severity_levels = $config->get('severity_levels');
      $log_value_list = $config->get('log_values');
      $method = $config->get('method') ?? 'exclude';
      $regex = $config->get('log_values_regex');
    }
    elseif ('syslog') {
      $severity_levels = $config->get('syslog_severity_levels');
      $log_value_list = $config->get('syslog_log_values');
      $method = $config->get('syslog_method') ?? 'exclude';
      $regex = $config->get('syslog_log_regex');
    }

    // Get RFC log levels.
    $levels = RfcLogLevel::getLevels();
    $reference_severity_level_list = [];
    foreach ($levels as $key => $log_level) {
      $reference_severity_level_list[$key] = strtolower($log_level->getUntranslatedString());
    }
    $current_level_text = $reference_severity_level_list[$current_level];

    $severity_levels_checked = [];
    foreach ($severity_levels as $level => $checked) {
      if ($checked) {
        $severity_levels_checked[] = $level;
      }
    }

    // If the severity level is checked do not calculate
    // the log values.
    if (!empty($severity_levels_checked)
      && in_array($current_level_text, $severity_levels_checked)
    ) {
      return ($method == 'include');
    }

    $match = FALSE;
    // Check for channel name and given values from log filter settings.
    foreach ($log_value_list as $row_log_value) {
      if ($this->rowLogValueMatch($row_log_value, $current_level_text, $context['channel'], $message, $regex)) {
        $match = TRUE;
        break;
      }
    }

    return ($method == 'include') ? $match : !$match;
  }

  /**
   * Check if level, channel and message match.
   */
  public function rowLogValueMatch($row_log_value, $current_level, $current_channel, $message, $regex) {
    $exploded_values = explode('|', $row_log_value);
    $severity_levels = $exploded_values[1];
    $channel = $exploded_values[0];
    $severity_level_list = explode(',', $severity_levels);

    // Check that the row belong to the current context channel
    // and that the severity level exists, if not, continue.
    if (!($channel == $current_channel
      && in_array($current_level, $severity_level_list))
    ) {
      return FALSE;
    }

    $matches = TRUE;
    // Check error level and message.
    // If isset the message, then check the expression.
    if (isset($regex[md5($row_log_value)])) {
      $regex_pattern = $regex[md5($row_log_value)];
      // Squash multiline strings to one line to allow for
      // simple regex matching.
      $message = preg_replace("/[\n\r]/", " ", $message);
      if (!preg_match($regex_pattern, $message)) {
        $matches = FALSE;
      }
    }

    return $matches;
  }

}
