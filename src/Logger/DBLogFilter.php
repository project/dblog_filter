<?php

namespace Drupal\dblog_filter\Logger;

use Drupal\dblog\Logger\DbLog;

/**
 * The DBLogFilter Class.
 */
class DBLogFilter extends DbLog {

  use LogFilterTrait;

  /**
   * {@inheritdoc}
   */
  public function log($level, string|\Stringable $message, array $context = []): void {
    if ($this->shouldLog('dblog', $level, $message, $context)) {
      parent::log($level, $message, $context);
    }
  }

}
