<?php

namespace Drupal\dblog_filter\Logger;

use Drupal\syslog\Logger\SysLog;

/**
 * Class SyslogFilter to add log messages.
 *
 * @package Drupal\dblog_filter
 */
class SyslogFilter extends SysLog {

  use LogFilterTrait;

  /**
   * {@inheritdoc}
   */
  public function log($level, string|\Stringable $message, array $context = []): void {
    if ($this->shouldLog('syslog', $level, $message, $context)) {
      parent::log($level, $message, $context);
    }
  }

}
