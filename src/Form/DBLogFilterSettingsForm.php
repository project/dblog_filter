<?php

namespace Drupal\dblog_filter\Form;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The DBLogFilterSettingsForm class.
 */
final class DBLogFilterSettingsForm extends ConfigFormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $moduleHandler) {
    parent::__construct($config_factory);
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dblog_filter_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['dblog_filter.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('dblog_filter.settings');
    $log_levels = RfcLogLevel::getLevels();
    $severity_levels = [];
    foreach ($log_levels as $log_level) {
      $log_level_value = $log_level->getUntranslatedString();
      $severity_levels[strtolower($log_level_value)] = $log_level_value;
    }

    // Dblog settings.
    if ($this->moduleHandler->moduleExists('dblog')) {
      $form['dblog'] = [
        '#type' => 'details',
        '#open' => TRUE,
        '#title' => $this->t('DB Log'),
      ];
      $severity_description = $this->t('If a severity level is checked then the value will override the value that match with the same level from the textarea.');
      $form['dblog']['severity_levels'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Select Severity Levels'),
        '#description' => $severity_description,
        '#options' => $severity_levels,
        '#default_value' => array_keys(array_filter($config->get('severity_levels'))),
      ];

      $description_log_values = $this->t(
        '<i>Severity Levels: emergency, alert, critical, error, warning, notice, info, debug</i>
            <br/>
            <i>Log Types: php, cron, mail, mymodule, etc.</i>
            <p>
            Enter one value per line, in the format log_type|level1,level2,level3..|message to limit the log messages stored, the message and the level are optionals.
            <br/>
            <strong>
              Example: cron|error,notice,warning|Cleaned up .* expired locks
              <br/>
              mymodule|error,notice
            </strong>
            </p>
            <p>
            <strong>Regular expressions are supported</strong>. You do not have to include the opening and closing forward slashes for the regular expression. The system will automatically add /^ and $/ at the beginning and end of the pattern to ensure that the match is always a full match instead of a partial match. This will help prevent unexpected filtering of messages. So if you want to filter out a specific message ensure that you add the full message including any punctuation and additional HTML if any. Add one per line. See <a href="@PCRE" target="_blank"> PCRE </a> documentation for details on regular expressions.
            </p>', [
              '@PCRE' => 'https://us3.php.net/manual/en/book.pcre.php',
            ]);

      $form['dblog']['method'] = [
        '#type' => 'radios',
        '#title' => $this->t('Method'),
        '#default_value' => $config->get('method') ?? 'include',
        '#options' => [
          'include' => $this->t('Only save selected or listed'),
          'exclude' => $this->t('Save all but selected or listed'),
        ],
      ];

      $log_values = $config->get('log_values') ? implode(PHP_EOL, $config->get('log_values')) : '';
      $form['dblog']['log_values'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Enter Log type and Severity Levels.'),
        '#default_value' => $log_values,
        '#description' => $description_log_values,
      ];
    }

    // Syslog settings.
    if ($this->moduleHandler->moduleExists('syslog')) {
      $form['syslog'] = [
        '#type' => 'details',
        '#open' => TRUE,
        '#title' => $this->t('Syslog'),
      ];
      $form['syslog']['syslog_severity_levels'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Select Severity Levels'),
        '#description' => $severity_description,
        '#options' => $severity_levels,
        '#default_value' => array_keys(array_filter($config->get('syslog_severity_levels'))),
      ];
      $syslog_log_values = $config->get('syslog_log_values') ? implode(PHP_EOL, $config->get('syslog_log_values')) : '';
      $form['syslog']['syslog_log_values'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Enter Log type and Severity Levels.'),
        '#default_value' => $syslog_log_values,
        '#description' => $description_log_values,
      ];
      $form['syslog']['syslog_method'] = [
        '#type' => 'radios',
        '#title' => $this->t('Method'),
        '#default_value' => $config->get('syslog_method') ?? 'include',
        '#options' => [
          'include' => $this->t('Only save selected or listed'),
          'exclude' => $this->t('Save all but selected or listed'),
        ],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save configurations.
    $values = $form_state->getValues();
    $config = $this->config('dblog_filter.settings');
    $fields = [
      'severity_levels',
      'log_values',
      'method',
      'syslog_severity_levels',
      'syslog_method',
      'syslog_log_values',
    ];
    foreach ($fields as $field) {
      if (isset($values[$field])) {
        if (in_array($field, ['log_values', 'syslog_log_values'])) {
          $log_value = array_filter(array_map('trim', explode(PHP_EOL, $values[$field])));
          $config->set($field, $log_value);
        }
        else {
          $config->set($field, $values[$field]);
        }

      }
    }

    $log_values = [
      'log_values',
      'syslog_log_values',
    ];

    foreach ($log_values as $key_log) {
      $log_values_form = $config->get($key_log) ?? [];
      self::setLogValuesRegex($log_values_form, $config, $key_log);
    }

    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Convert to regex value.
   */
  public static function setLogValuesRegex(array $log_values_list, Config $config, string $key) {
    $log_values_regex = [];
    // Process and save the regular expressions in another variable.
    foreach ($log_values_list as $log_value) {
      $explode_values = explode('|', $log_value);
      if (isset($explode_values[2])) {
        $pattern = $explode_values[2];
        $pattern = preg_replace(['/^\s*/', '/\s*$/'], '', $pattern);
        $regex = '/^' . $pattern . '$/i';
        $log_values_regex[md5($log_value)] = $regex;
      }
    }
    $config->set($key . '_regex', $log_values_regex);
  }

}
