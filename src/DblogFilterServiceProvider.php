<?php

namespace Drupal\dblog_filter;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class MyModuleServiceProvider.
 *
 * @package Drupal\dblog_filter
 */
class DblogFilterServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->has('logger.dblog')) {
      $definition = $container->getDefinition('logger.dblog');
      $definition->setClass('Drupal\dblog_filter\Logger\DBLogFilter');
      $definition->setArguments(
        [
          new Reference('database'),
          new Reference('logger.log_message_parser'),
        ]
      );
    }
    if ($container->has('logger.syslog')) {
      $definition = $container->getDefinition('logger.syslog');
      $definition->setClass('Drupal\dblog_filter\Logger\SyslogFilter');
      $definition->setArguments(
        [
          new Reference('config.factory'),
          new Reference('logger.log_message_parser'),
        ]
      );
    }
  }

}
